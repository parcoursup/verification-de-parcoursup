#!/bin/bash

scriptdir=$(cd $(dirname $0); pwd)
export PROJECT_BUILD_DIRECTORY=${scriptdir}/../target
MAKEFILE=${scriptdir}/generate-java-sources.mk
WHYML_SOURCE_DIR="src/main/whyml"

case $1 in
  *".mlw") make -f ${MAKEFILE} $1-ide ;;
  *) find ${WHYML_SOURCE_DIR} -name $1.mlw -exec make -f ${MAKEFILE} {}-ide \;
esac

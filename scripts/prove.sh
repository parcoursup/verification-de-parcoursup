#!/bin/bash

scriptdir=$(cd $(dirname $0); pwd)
topdir=$(cd ${scriptdir}/..; pwd)
builddir="${BUILDDIR}"

if test "${NB_JOBS_PER_FILE}" = ""; then
  NB_JOBS_PER_FILE=4
fi

INPUT_FILE=$1

TMPDIR="${builddir}/tmp/$$"
AE_DIR=${TMPDIR}/ae
CVC5_DIR=${TMPDIR}/cvc5
Z3_DIR=${TMPDIR}/z3

mkdir -p ${AE_DIR} ${CVC5_DIR} ${Z3_DIR}

trap "rm -fr ${TMPDIR}; exit 1" INT

set -e

why3 prove ${LIBDIRS} -P alt-ergo -a split_vc -o ${AE_DIR} ${OPTIONS} ${INPUT_FILE}
why3 prove ${LIBDIRS} -P cvc5 -a split_vc -o ${CVC5_DIR} ${OPTIONS} ${INPUT_FILE}
why3 prove ${LIBDIRS} -P z3 -a split_vc -o ${Z3_DIR} ${OPTIONS} ${INPUT_FILE}

NB_AE_FILES=$(find ${AE_DIR} -name '*.ae' | wc -l)
NB_CVC5_FILES=$(find ${CVC5_DIR} -name '*.smt2' | wc -l)
NB_Z3_FILES=$(find ${Z3_DIR} -name '*.smt2' | wc -l)
if test ${NB_AE_FILES} -ne ${NB_CVC5_FILES} -o ${NB_AE_FILES} -ne ${NB_Z3_FILES}; then
	cat 1>&2 <<EOF
Solvers don't generate the same number of goals. Have a look into:
${NB_AE_FILES} in ${AE_DIR}
${NB_CVC5_FILES} in ${CVC5_DIR}
${NB_Z3_FILES} in ${Z3_DIR}
EOF
	exit 1
fi

echo "all : \\" > ${TMPDIR}/Makefile

echo > ${TMPDIR}/Makefile.rules

for f in $(find ${AE_DIR} -name '*.ae'); do
	AE_IN=$f
	filename=$(basename $f .ae)
	CVC5_IN=${CVC5_DIR}/${filename}.smt2
	Z3_IN=${Z3_DIR}/${filename}.smt2
	if test ! -f ${CVC5_IN}; then
			echo "missing goal file ${filename} for CVC5 solver" 1>&2
			exit 1
	fi
	if test ! -f ${Z3_IN}; then
			echo "missing goal file ${filename} for Z3 solver" 1>&2
			exit 1
	fi
	echo " ${TMPDIR}/${filename}.results \\" >> ${TMPDIR}/Makefile

  echo "${TMPDIR}/${filename}.results : ${AE_IN} ${CVC5_IN} ${Z3_IN}" >> ${TMPDIR}/Makefile.rules
  printf "\t@ ${scriptdir}/solver.sh ${TMPDIR}/${filename}.results ${AE_IN} ${CVC5_IN} ${Z3_IN}\n" >> ${TMPDIR}/Makefile.rules

done
cat >> ${TMPDIR}/Makefile <<EOF
 \${dummy}

include ${TMPDIR}/Makefile.rules

EOF

case "${MAKEFLAGS}" in
  *"jobserver"*) ;;
  *) MAKEFLAGS="-j ${NB_JOBS_PER_FILE}" ;;
esac

make -f ${TMPDIR}/Makefile  ${MAKEFLAGS} all

if test $? != 0; then
  grep -r Failure ${TMPDIR}
  if test "${IDE_ON_FAILURE}" = "yes"; then
    ${scriptdir}/ide.sh ${INPUT_FILE}
  fi
  exit 1
fi

#!/bin/bash

OUTPUT_DIR="$1"
WHYML_SOURCE_DIR="src/main/whyml"

excluded_java_files="^${WHYML_SOURCE_DIR}/proofs/.*$ .*_clone.mlw"
excluded_proof_files=""

classnames="${OUTPUT_DIR}/Makefile.classnames"
proof_files="${OUTPUT_DIR}/Makefile.proofs"
deps="${OUTPUT_DIR}/Makefile.deps"

camelcasecmd() {
    echo $1 | perl -pe 's/(^|_)([a-z])/uc($2)/ge'
}

echo "CLASSNAMES=\\" > ${classnames}
echo "PROOF_FILES=\\" > ${proof_files}
echo > ${deps}

for f in $(find ${WHYML_SOURCE_DIR} -name '[a-zA-z]*.mlw'); do
    prefixpath=$(dirname $f | sed -e "s+^${WHYML_SOURCE_DIR}/++g")
    classname=$(camelcasecmd $(basename $f ".mlw"))
    fullclassname=$(echo $prefixpath | sed -e 's+/+.+g').${classname}
    classfile=$(camelcasecmd $(basename $f ".mlw")).java

    generate_java=true
    generate_proofs=true

    for p in ${excluded_java_files}; do
        if [[ $f =~ $p ]]; then
            generate_java=false
            break
        fi
    done

    for p in ${excluded_proof_files}; do
        if [[ $f =~ $p ]]; then
            generate_proofs=false
            break
        fi
    done

    if ${generate_java}; then
        echo "${fullclassname} \\" >> ${classnames}
        
        cat >> ${deps} <<EOF
check-${fullclassname} : $f
	@ \${WHY3_CHECK} \$< 

\$(GENERATED_SOURCE_DIR)/$prefixpath/$classfile : $f \${LOCAL_DRIVERS}
	@ \$(DO_EXTRACTION)

EOF
    else
        echo "ignore $f for Java"
    fi

    proof_file="${prefixpath}/$(basename $f .mlw).proofs"
    if ${generate_proofs}; then
        echo "\$(GENERATED_PROOFS_DIR)/${proof_file} \\" >> ${proof_files}
        cat >> ${deps} <<EOF
$f-proofs : \$(GENERATED_PROOFS_DIR)/${proof_file}

\$(GENERATED_PROOFS_DIR)/${proof_file} : $f
	@ \$(DO_PROOFS)
	@ echo "File proved" > \$@

$f-ide:
	@ \${WHY3_IDE} $f

EOF
    else
        echo "ignore $f for proofs"
    fi

done
echo "\${dummy}" >> ${classnames}
echo "\${dummy}" >> ${proof_files}


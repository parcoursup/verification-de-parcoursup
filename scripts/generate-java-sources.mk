DEBUG_ON=--debug extraction --debug java_extraction
CHECK_WHYML=

BUILDDIR=${PROJECT_BUILD_DIRECTORY}
SCRIPTSDIR=scripts
WHYML_SOURCE_DIR=src/main/whyml
GENERATED_SOURCE_DIR=${BUILDDIR}/generated-sources
GENERATED_PROOFS_DIR=${BUILDDIR}/proofs
TOP_PACKAGE_PATH=${GENERATED_SOURCE_DIR}
TOP_PROOFS_DIR=${GENERATED_PROOFS_DIR}
HTMLDOCDIR=${BUILDDIR}/html

include ${BUILDDIR}/Makefile.classnames
include ${BUILDDIR}/Makefile.proofs

CLASSPATHS=$(subst .,/,${CLASSNAMES})

JAVA_GENERATED_FILES=$(CLASSPATHS:%=${GENERATED_SOURCE_DIR}/%.java)
CLASS_FILES= \
 $(JAVA_GENERATED_FILES:%.java=%.class) \
 $(TOP_PACKAGE_PATH)/fr/parcoursup/whyml/ordreappel/algo/VoeuClasse\$$TypeCandidat.class \
 ${TOP_PACKAGE_PATH}/fr/parcoursup/whyml/exceptions/VerificationExceptionMessage\$$T.class


LIBDIRS=-L ${WHYML_SOURCE_DIR}
LOCAL_DRIVERS=${BUILDDIR}/proofs.drv drv/pqueue_voeu_classe.drv drv/sorted_lists.drv

WHY3_EXTRACT_JAVA= \
  why3 ${DEBUG_${DEBUG_MODE}} extract \
  -D java ${LOCAL_DRIVERS:%=-D %} ${LIBDIRS} --modular --recursive

WHY3_PROVE=LIBDIRS="${LIBDIRS}" NB_JOBS_PER_FILE=4 BUILDDIR=${BUILDDIR} ${SCRIPTSDIR}/prove.sh
WHY3_IDE=why3 ide ${LIBDIRS}
WHY3_DOC=why3 doc -L . -o . --index --title="Parcoursup / Calcul de l'ordre d'appel des candidats"

GENERATED_FILES= \
  ${JAVA_GENERATED_FILES} \
  ${JAVA_GENERATED_FILES:%.java=%.java.bak} \
  ${CLASS_FILES} \
  ${JAVA_GENERATED_FILES:%.java=%.java-err} \
  ${CLASS_FILES:%.class=%.class-err} \
  ${PROOF_FILES} ${PROOF_FILES=%.proofs:%.proofs-err} \
  ${BUILDDIR}/proofs.drv


DO_EXTRACTION= \
 mkdir -p $$(dirname $@); \
 pkg=$$(dirname $< | sed -e "s+${WHYML_SOURCE_DIR}/++g" -e 's+/+.+g' ); \
 module=$${pkg}.$$(basename $< .mlw).$$(basename $@ .java); \
 echo "${WHY3_EXTRACT_JAVA} -o ${GENERATED_SOURCE_DIR} $${module}"; \
 ${WHY3_EXTRACT_JAVA} -o ${GENERATED_SOURCE_DIR} $${module} 2> $@-err; \
 if test $$? = 0; then \
   cat -n $@; \
 else \
   echo "an error occurs"; \
   cat $@-err; \
   exit 1; \
 fi

DO_PROOFS= \
  mkdir -p $$(dirname $@); \
  echo "Proving $<"; \
  ${WHY3_PROVE} $<



all: Makefile.deps Makefile.proofs Makefile.classnames ${CLASS_FILES} proofs

java-sources : ${JAVA_GENERATED_FILES}

proofs: ${PROOF_FILES}

local-drivers: ${LOCAL_DRIVERS}

${BUILDDIR}/proofs.drv: ${SCRIPTSDIR}/genproofdrv.sh
	mkdir -p ${BUILDDIR}
	${SCRIPTSDIR}/genproofdrv.sh > $@

check-all: ${CLASSNAMES:%=check-%}

clean:
	@ echo cleaning generated files; \
 	  rm -fr ${GENERATED_FILES}

clean-all: clean
	@ echo cleaning created directories; \
  	  rm -fr ${GENERATED_SOURCE_DIR} ${GENERATED_PROOFS_DIR}
	@ echo cleaning up session directories;\
 	  for f in $$(find ${WHYML_SOURCE_DIR} -name '*.mlw'); do \
 	    d="$$(dirname $$f)/$$(basename $$f '.mlw')"; \
  	   if test -d $$d; then \
  	     rmdir $$d; \
	   fi; \
	  done

html-doc:
	@ echo generating html documentation
	@ rm -fr ${HTMLDOCDIR}; mkdir -p ${HTMLDOCDIR}
	@ cp -r ${WHYML_SOURCE_DIR}/* ${HTMLDOCDIR}
	@ for f in $$(find ${HTMLDOCDIR} -name '*.mlw'); do \
   		(echo; echo; echo "(* generated on $$(date) from rev: $$(git rev-parse HEAD) *)" ) >> $$f; \
      done
	cd ${HTMLDOCDIR}; ${WHY3_DOC} $$(find . -name '*.mlw' | sed -e 's=^./==g');

%.class : %.java
	@- echo JAVAC $<
	@ javac -cp $(GENERATED_SOURCE_DIR) $< 2> $@-err ; \
          if test $$? != 0 ; then cat $@-err ; exit 1; fi 

Makefile.deps Makefile.proofs Makefile.classnames : ${SCRIPTSDIR}/gendeps.sh
	${SCRIPTSDIR}/gendeps.sh

include ${BUILDDIR}/Makefile.deps

${TOP_PACKAGE_PATH}/fr/parcoursup/whyml/ordreappel/algo/VoeuClasse\$$TypeCandidat.class : \
  ${TOP_PACKAGE_PATH}/fr/parcoursup/whyml/ordreappel/algo/VoeuClasse.class

${TOP_PACKAGE_PATH}/fr/parcoursup/whyml/exceptions/VerificationExceptionMessage\$$T.class : \
  ${TOP_PACKAGE_PATH}/fr/parcoursup/whyml/exceptions/VerificationExceptionMessage.class

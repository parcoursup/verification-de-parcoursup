#!/bin/bash

TIMEOUTS="5 10 20 60"

RESFILE="$1"
AE_IN="$2"
CVC5_IN="$3"
Z3_IN="$4"

GOAL=$(basename ${AE_IN} ".ae")

exec 5<>${RESFILE}

exec 2>&5

AE() {
  alt-ergo -t $1 ${AE_IN} 2>&1 > ${AE_IN}.out
}

CVC5() {
  cvc5 --tlimit ${1}000 ${CVC5_IN} 2>&1 > ${CVC5_IN}.out
  if grep -q "unsat" "${CVC5_IN}.out"; then
  	return 0
  else
    return 1
  fi
}

Z3() {
  z3 -smt2 -T:$1 ${Z3_IN} 2>&1 > ${Z3_IN}.out
  if grep -q "unsat" "${Z3_IN}.out"; then
  	return 0
  else
    return 1
  fi
}

print() {
  printf "$1\n"
  printf "$1\n" >> ${RESFILE}
}

success() {
  print "${GOAL} \e[32mSuccess (with $1 before $2 s)\e[0m."
  exit 0
}

timeout() {
  print "${GOAL} \e[36mTimeout of $1 after $2 s\e[0m."
}

failure() {
  print "${GOAL} \e[91mFailure\e[0m."
  exit 1
}

trap "exit 1" INT

for T in ${TIMEOUTS}; do
  for solver in AE CVC5 Z3; do
    if ${solver} ${T}; then
      success "${solver}" $T
    else
      timeout "${solver}" $T
    fi
  done
done

failure


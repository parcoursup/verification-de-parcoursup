#!/bin/bash


WHYML_SOURCE_DIR="src/main/whyml"
WHYML_PROOFS_DIR="${WHYML_SOURCE_DIR}/proofs"

for ml in $(grep -re "^module [A-Z].*$" ${WHYML_PROOFS_DIR} | sed -e "s+${WHYML_SOURCE_DIR}/++g" -e 's/.mlw//g' -e 's+/+.+g' -e 's/: *module /./g'); do
  cat << EOF
module $ml
  remove module
end

EOF
done
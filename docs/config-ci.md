### Setting continuous integration

* Create a new gitlab-runner on runners host 
  * Go to > Settings > CI/CD > Runners. Copy the given registration token
  * On the runners' host, register a new gitlab-runner
    * Gitlab instance URL: https://gitlab.mim-libre.fr/
    * Registration token: the one given by the forge
    * Description of the runner: runner-verification-de-parcoursup
    * Tags: _empty_
    * Optional maintenance note: _empty_
    * Executor: docker
    * Default Docker image: debian:stable
  * Go to > Settings > CI/CD. The runner should appear.
* Add deployment token
  * Go to > Settings > Repository and create a new deploy token
      * Name: `gitlab-maven-verification-parcoursup-deploy-token`
      * Check boxes: `read_package_registry` and `write_package_registry`
      * Copy/paste the given password
  * Go to > Settings > Variables (this variable is used in file [ci_settings.xml](/ci_settings.xml))
    * Add the variable `PSUP_CI_PACKAGE_TOKEN`
    * Set the value to the password of the deployment token 
    * Check all protection boxes
    

[![Pipeline Status](https://gitlab.mim-libre.fr/parcoursup/verification-de-parcoursup/badges/master/pipeline.svg?ignore_skipped=true "Pipeline status")](https://gitlab.mim-libre.fr/parcoursup/verification-de-parcoursup/-/pipelines)

# Algorithms of Parcoursup: specification, proofs and code generation

This repository provides an implementation of one of the many algorithms of [Parcoursup](https://www.parcoursup.fr): 
the computation of the *calling orders* (named *ordres d'appel* in French). This algorithm is defined in section 4
of [Document de Présentation des Algorithmes de Parcoursup](https://gitlab.mim-libre.fr/parcoursup/algorithmes-de-parcoursup/-/blob/master/doc/presentation_algorithmes_parcoursup_2023.pdf).
The definition of the algorithm in pseudocode comes with an informal specification
of the algorithm, as a list of properties provided at the end of sections 4.1 and 4.2
of the aforementioned document. The implementation of the algorithm is written
in Java and its source code is freely available on 
[M.I.M Libre](https://gitlab.mim-libre.fr/parcoursup/algorithmes-de-parcoursup.git).

The algorithm has been partially proved using different tools [*openjml*](https://www.openjml.org), 
[*Prusti*](https://analysis-tools.dev/tool/prusti) and jml2why3 ([LRI](https://why3.lri.fr)). 
These works are documented in https://hal.inria.fr/hal-02447409/document.

In 2022 [a complete proof](https://hal.science/hal-03388671/document) of the whole specification as been written in 
WhyML, the specification language of the [Why3 tool](https://why3.lri.fr). In this work the algorithm remains written
in a programming language different from the one used in production i.e. Java. [Why3 tool](https://why3.lri.fr) permits
to compile (or extract) WhyML programs into OCaml and C languages. A new [Why3](https://why3.lri.fr) driver has been 
written to extract Java programs from WhyML; this work and a [short documentation](https://gitlab.inria.fr/why3/why3/-/blob/whyml2java/doc/whyml2java.rst)
of the driver can be found on the `whyml2java` branch of the
[Why3 repository](https://gitlab.inria.fr/why3/why3/-/tree/whyml2java). 

The Java driver permitted us to generate an almost complete implementation of the algorithm from a new version of the
WhyML proof. This repository provides this new proof and its compilation into a usable Java package; this latter is also 
available from the [package registry](https://gitlab.mim-libre.fr/parcoursup/verification-de-parcoursup/-/packages).

# Replaying proofs and generating the Java package 

In order to replay proofs you need to install:
* the [Why3 tool](https://why3.lri.fr) from the `whyml2java` branch of its [Git repository](https://gitlab.inria.fr/why3/why3/-/tree/whyml2java).
* and the three SMT-solvers: [alt-ergo](https://alt-ergo.ocamlpro.com/), [CVC4](https://cvc4.github.io/) and  [Z3](https://github.com/Z3Prover/z3).

Finally, make sure that Why3 has detected the solvers (using the command `why3 config detect`).

Then from the root of the project, just run the command `make proofs`. After the proofs are completed, the Java code is 
generated into `target/generated-sources`. To build the `jar` just type `make jar`; the binary file, 
`parcoursup-algos-whyml-1-0.jar`, is located into the `target` directory.

The documentation generated with `why3 doc` is available at the following address: 
https://parcoursup.labri.fr/why3/calcul_ordre_appel/

but you can recompile it using the command `make doc`; in this case you will find the html files into the directory `target/html`.

# Organization of the code and proofs

The entire code of the algorithm is located in the directory [src/main/whyml](src/main/whyml). The WhyML files are dispatched
into two subdirectories:
* [`fr/parcoursup/whyml`](src/main/whyml/fr/parcoursup/whyml) contains source files that actually implement the algorithm and its data structures. The 
directory itself is organized with respect to generated packages. Each WhyMl file in this directory is used to produce
a Java class (except files with the suffix `_clone.mlw`); for instance 
`fr/parcoursup/whyml/ordreappel/calcul_ordre_appel.mlw` yields the class `fr.parcoursup.whyml.ordreappel.CalculOrdreAppel`. 
The Java files are generated in the directory `target/generated-sources`.

* and [`proofs`](src/main/whyml/proofs) contains files used for specifications and proofs. [`proofs/lib`](src/main/whyml/proofs/lib) contains generic modules while 
those specific to the algorithm (e.g. invariants) are stored under [`proofs/ordre_appel`](src/main/whyml/proofs/ordre_appel).

# Description of the repository

The repository is organized as follows:
* [pom.xml](pom.xml) permits to run proofs, to generate Java code and to package it.
* [Makefile](Makefile)'s targets are essentially shortcut to Maven (`mvn`) commands.
* [.gitlab-ci.yml](.gitlab-ci.yml) and [ci_settings.xml](ci_settings.xml) are configuration files used the continuous 
integration work flow.
* [scripts](scripts) directory contains internal scripts used to replay the proof or to extract the Java code. 
  [scripts/ide.sh](scripts/ide.sh) can be used to launch Why3's IDE on a WhyML module.
* [drv](drv) contains specific driver files used to extract Java code from the proof.
* [src](src) is the main source code directory. It follows Maven convention:
  * [src/main/java](src/main/java) contains non-generated Java code.
  * [src/main/whyml](src/main/whyml) contains the entire WhyML code i.e., both the algorithm implementation and its 
    proof. 
  * [src/test/java](src/test/java) contains non-generated Java code used by tests.
  * [src/test/resources](src/test/resources) contains input data used by tests.

# Contributors

Joint project between [LRI](https://www.lri.fr) and [LaBRI](https://www.labri.fr) with support from 
[MESRI](https://www.enseignementsup-recherche.gouv.fr), [CNRS](https://www.cnrs.fr) and [Inria](https://www.inria.fr).

* Léo Andrès
* Benedikt Becker
* Pierre Castéran
* Jean-Christophe Filliâtre
* Hugo Gimbert 
* Claude Marché
* Gérald Point

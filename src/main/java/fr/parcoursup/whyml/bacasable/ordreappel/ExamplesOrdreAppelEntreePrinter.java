package fr.parcoursup.whyml.bacasable.ordreappel;

import fr.parcoursup.whyml.util.AlgoOrdreAppelDonneesBuilder;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class ExamplesOrdreAppelEntreePrinter {
    static AlgoOrdreAppelDonneesBuilder example_01() {
//        GroupeClassement groupe = new GroupeClassement(0, 20, 0);
//        /* C1 C2 C3 C4 C5 B6 B7 C8 */
//        for (int i = 1; i <= 5; i++) {
//            groupe.ajouterVoeu(new VoeuClasse(i, i, false, false));
//        }
//        groupe.ajouterVoeu(new VoeuClasse(6, 6, true, false));
//        groupe.ajouterVoeu(new VoeuClasse(7, 7, true, false));
//        groupe.ajouterVoeu(new VoeuClasse(8, 8, false, false));
        AlgoOrdreAppelDonneesBuilder builder = new AlgoOrdreAppelDonneesBuilder();

        builder.newGroup();
        builder.setCGpCod(0);
        builder.setTauxBoursier(20);
        builder.setTauxResident(0);

        for (int i = 1; i <= 5; i++) {
            builder.addVoeu(i, i, false, false);
        }
        builder.addVoeu(6, 6, true, false);
        builder.addVoeu(7, 7, true, false);
        builder.addVoeu(8, 8, false, false);

        return builder;
    }

    static AlgoOrdreAppelDonneesBuilder example_02() {
//        GroupeClassement groupe = new GroupeClassement(0, 2, 0);
//
//        /* C1 C2 C3 C4 C5 B6 C7 C8 */
//        for (int i = 1; i <= 5; i++) {
//            groupe.ajouterVoeu(new VoeuClasse(i, i, false, false));
//        }
//        groupe.ajouterVoeu(new VoeuClasse(6, 6, true, false));
//        groupe.ajouterVoeu(new VoeuClasse(7, 7, false, false));
//        groupe.ajouterVoeu(new VoeuClasse(8, 8, false, false));
        AlgoOrdreAppelDonneesBuilder builder = new AlgoOrdreAppelDonneesBuilder();

        builder.newGroup();
        builder.setCGpCod(0);
        builder.setTauxBoursier(2);
        builder.setTauxResident(0);
        for (int i = 1; i <= 5; i++) {
            builder.addVoeu(i, i, false, false);
        }
        builder.addVoeu(6, 6, true, false);
        builder.addVoeu(7, 7, false, false);
        builder.addVoeu(8, 8, false, false);

        return builder;
    }

    static AlgoOrdreAppelDonneesBuilder example_03() {
//        GroupeClassement groupe = new GroupeClassement(0, 10, 0);
//        /* C1 C2 C3 ...C900 B901 B902 B903 ...B1000 */
//        for (int i = 1; i <= 900; i++) {
//            groupe.ajouterVoeu(new VoeuClasse(i, i, false, false));
//        }
//        for (int i = 901; i <= 1_000; i++) {
//            groupe.ajouterVoeu(new VoeuClasse(i, i, true, false));
//        }
        AlgoOrdreAppelDonneesBuilder builder = new AlgoOrdreAppelDonneesBuilder();

        builder.newGroup();
        builder.setCGpCod(0);
        builder.setTauxBoursier(10);
        builder.setTauxResident(0);
        for (int i = 1; i <= 900; i++) {
            builder.addVoeu(i, i, false, false);
        }
        for (int i = 901; i <= 1_000; i++) {
            builder.addVoeu(i, i, true, false);
        }

        return builder;
    }

    static AlgoOrdreAppelDonneesBuilder example_04() {
//        GroupeClassement groupe = new GroupeClassement(0, 10, 0);
//
//        /* C1 B2 B3 C4 C5 C6 C7 B8 C9 C10 */
//        groupe.ajouterVoeu(new VoeuClasse(1, 1, false, false));
//        groupe.ajouterVoeu(new VoeuClasse(2, 2, true, false));
//        groupe.ajouterVoeu(new VoeuClasse(3, 3, true, false));
//        groupe.ajouterVoeu(new VoeuClasse(4, 4, false, false));
//        groupe.ajouterVoeu(new VoeuClasse(5, 5, false, false));
//        groupe.ajouterVoeu(new VoeuClasse(6, 6, false, false));
//        groupe.ajouterVoeu(new VoeuClasse(7, 7, false, false));
//        groupe.ajouterVoeu(new VoeuClasse(8, 8, true, false));
//        groupe.ajouterVoeu(new VoeuClasse(9, 9, false, false));
//        groupe.ajouterVoeu(new VoeuClasse(10, 10, false, false));

        AlgoOrdreAppelDonneesBuilder builder = new AlgoOrdreAppelDonneesBuilder();

        builder.newGroup();
        builder.setCGpCod(0);
        builder.setTauxBoursier(10);
        builder.setTauxResident(0);
        builder.addVoeu(1, 1, false, false);
        builder.addVoeu(2, 2, true, false);
        builder.addVoeu(3, 3, true, false);
        builder.addVoeu(4, 4, false, false);
        builder.addVoeu(5, 5, false, false);
        builder.addVoeu(6, 6, false, false);
        builder.addVoeu(7, 7, false, false);
        builder.addVoeu(8, 8, true, false);
        builder.addVoeu(9, 9, false, false);
        builder.addVoeu(10, 10, false, false);

        return builder;
    }

    static AlgoOrdreAppelDonneesBuilder example_05() {
//        GroupeClassement groupe = new GroupeClassement(0, 10, 95);
//
//        /*  (BR)1(BR)2R3 . . . R19C20
//            (BR)21(BR)22R23 . . . R39C40
//            (BR)41R42 . . . R50
//            B51R52 . . . R60
//            B61R62 . . . R70
//            B71R72 . . . R80
//            B81R82 . . . R90
//            B91R92 . . . R100. */
//        for (int i = 0; i <= 1; i++) {
//            groupe.ajouterVoeu(new VoeuClasse(20 * i + 1, 20 * i + 1, true, true));
//            groupe.ajouterVoeu(new VoeuClasse(20 * i + 2, 20 * i + 2, true, true));
//            for (int j = 3; j <= 19; j++) {
//                groupe.ajouterVoeu(new VoeuClasse(20 * i + j, 20 * i + j, false, true));
//            }
//            groupe.ajouterVoeu(new VoeuClasse(20 * i + 20, 20 * i + 20, false, false));
//        }
//
//        groupe.ajouterVoeu(new VoeuClasse(41, 41, true, true));
//        for (int k = 42; k <= 50; k++) {
//            groupe.ajouterVoeu(new VoeuClasse(k, k, false, true));
//        }
//
//        for (int l = 51; l <= 91; l += 10) {
//            groupe.ajouterVoeu(new VoeuClasse(l, l, true, false));
//            for (int m = l + 1; m <= l + 9; m++) {
//                groupe.ajouterVoeu(new VoeuClasse(m, m, false, true));
//            }
//        }

        AlgoOrdreAppelDonneesBuilder builder = new AlgoOrdreAppelDonneesBuilder();

        builder.newGroup();
        builder.setCGpCod(0);
        builder.setTauxBoursier(10);
        builder.setTauxResident(95);

        for (int i = 0; i <= 1; i++) {
            builder.addVoeu(20 * i + 1, 20 * i + 1, true, true);
            builder.addVoeu(20 * i + 2, 20 * i + 2, true, true);
            for (int j = 3; j <= 19; j++) {
                builder.addVoeu(20 * i + j, 20 * i + j, false, true);
            }
            builder.addVoeu(20 * i + 20, 20 * i + 20, false, false);
        }

        builder.addVoeu(41, 41, true, true);
        for (int k = 42; k <= 50; k++) {
            builder.addVoeu(k, k, false, true);
        }

        for (int l = 51; l <= 91; l += 10) {
            builder.addVoeu(l, l, true, false);
            for (int m = l + 1; m <= l + 9; m++) {
                builder.addVoeu(m, m, false, true);
            }
        }

        return builder;
    }

    static AlgoOrdreAppelDonneesBuilder example_06() {
//        GroupeClassement groupe = new GroupeClassement(0, 10, 95);
//
//        for (int i = 1; i <= 100; i++) {
//            groupe.ajouterVoeu(new VoeuClasse(i, i, false, true));
//        }
//
//        for (int i = 101; i <= 111; i++) {
//            groupe.ajouterVoeu(new VoeuClasse(i, i, true, false));
//        }

        AlgoOrdreAppelDonneesBuilder builder = new AlgoOrdreAppelDonneesBuilder();

        builder.newGroup();
        builder.setCGpCod(0);
        builder.setTauxBoursier(10);
        builder.setTauxResident(95);
        for (int i = 1; i <= 100; i++) {
            builder.addVoeu(i, i, false, true);
        }

        for (int i = 101; i <= 111; i++) {
            builder.addVoeu(i, i, true, false);
        }

        return builder;
    }

    static AlgoOrdreAppelDonneesBuilder example_07() {
//        GroupeClassement groupe = new GroupeClassement(0, 17, 79);
//
//        groupe.ajouterVoeu(new VoeuClasse(1, 1, true, false));
//        groupe.ajouterVoeu(new VoeuClasse(2, 2, false, false));
//        groupe.ajouterVoeu(new VoeuClasse(3, 3, true, true));
//        groupe.ajouterVoeu(new VoeuClasse(4, 4, false, false));
//        groupe.ajouterVoeu(new VoeuClasse(5, 5, false, false));
//        groupe.ajouterVoeu(new VoeuClasse(6, 6, false, true));

        AlgoOrdreAppelDonneesBuilder builder = new AlgoOrdreAppelDonneesBuilder();

        builder.newGroup();
        builder.setCGpCod(0);
        builder.setTauxBoursier(17);
        builder.setTauxResident(79);
        builder.addVoeu(1, 1, true, false);
        builder.addVoeu(2, 2, false, false);
        builder.addVoeu(3, 3, true, true);
        builder.addVoeu(4, 4, false, false);
        builder.addVoeu(5, 5, false, false);
        builder.addVoeu(6, 6, false, true);

        return builder;
    }

    static AlgoOrdreAppelDonneesBuilder two_hundred_groups_no_boursiers () {
        AlgoOrdreAppelDonneesBuilder builder = new AlgoOrdreAppelDonneesBuilder();

        for(int j = 0 ; j < 200 ; j++) {
            builder.newGroup();
            builder.setCGpCod(j);
            builder.setTauxBoursier(50);
            builder.setTauxResident(0);

            for (int i = 0 ; i < 1; i++) {  // Inutile de boucler sur les voeux, mais je le laisse au cas où
                builder.addVoeu(i, 1 + i, false, false);
            }
        }

        return builder;
    }

    static AlgoOrdreAppelDonneesBuilder two_hundred_groups_no_boursiers_with_ra () {
        AlgoOrdreAppelDonneesBuilder builder = new AlgoOrdreAppelDonneesBuilder();

        for(int j = 0 ; j < 200 ; j++) {
            builder.newGroup();
            builder.setCGpCod(j);
            builder.setTauxBoursier(50);
            builder.setTauxResident(0);

            for (int i = 0 ; i < 1; i++) {  // Inutile de boucler sur les voeux, mais je le laisse au cas où
                builder.addVoeu(i, 1 + i, i+1,false, false);
            }
        }

        return builder;
    }

    public static void main(String[] args) throws Exception {
        String output = "src/test/resources/non_regression/example_01.json";
        System.out.println(output);
        example_01().writeToPrintStream(new PrintStream(output), false);

        output = "src/test/resources/non_regression/example_02.json";
        System.out.println(output);
        example_02().writeToPrintStream(new PrintStream(output), false);

        output = "src/test/resources/non_regression/example_03.json";
        System.out.println(output);
        example_03().writeToPrintStream(new PrintStream(output), false);

        output = "src/test/resources/non_regression/example_04.json";
        System.out.println(output);
        example_04().writeToPrintStream(new PrintStream(output), false);

        output = "src/test/resources/non_regression/example_05.json";
        System.out.println(output);
        example_05().writeToPrintStream(new PrintStream(output), false);

        output = "src/test/resources/non_regression/example_06.json";
        System.out.println(output);
        example_06().writeToPrintStream(new PrintStream(output), false);

        output = "src/test/resources/non_regression/example_07.json";
        System.out.println(output);
        example_07().writeToPrintStream(new PrintStream(output), false);

        output = "src/test/resources/verification_resultats/two_hundred_groups_no_boursiers.json";
        System.out.println(output);
        two_hundred_groups_no_boursiers().writeToPrintStream(new PrintStream(output), false);

        output = "src/test/resources/verification_resultats/two_hundred_groups_no_boursiers-result.json";
        System.out.println(output);
        two_hundred_groups_no_boursiers_with_ra().writeToPrintStream(new PrintStream(output), false);
    }
}

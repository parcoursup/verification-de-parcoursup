package fr.parcoursup.whyml.util;

import com.google.gson.GsonBuilder;
import fr.parcoursup.whyml.ordreappel.algo.AlgoOrdreAppelSortie;
import fr.parcoursup.whyml.ordreappel.algo.GroupeClassementValide;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class OrdreAppelSortieBuilder {
    private static class Candidat {
        int gCnCod;
        int rangAppel;

        Candidat(int gCnCod, int rangAppel) {
            this.gCnCod = gCnCod;
            this.rangAppel = rangAppel;
        }

        int[] toRawData() {
            return new int[] { gCnCod, rangAppel };
        }

        boolean equalsRawData (int[] data) throws InvalidDataException {
            if (data == null || data.length != 2) {
                throw new InvalidDataException("bad json for CandidatClasse");
            }
            return gCnCod == data[0] && rangAppel == data[1];
        }
    }
    private static class OrdreAppel {
        int cGpCod = 0;
        List<Candidat> ordre_appel = new ArrayList<>();

        public void addCandidat(int gCnCod, int rang_appel) {
            ordre_appel.add(new Candidat(gCnCod, rang_appel));
        }

        public int[][] toRawData() {
            int[][] result = new int[1 + ordre_appel.size()][];
            int offset = 0;
            result[offset++] = new int[] { cGpCod };
            for(Candidat c : ordre_appel)
                result[offset++] = c.toRawData();
            return result;
        }

        public boolean equalsRawData(int [][] data) throws InvalidDataException {
            if (data == null || data.length != ordre_appel.size() + 1 ) {
                throw new InvalidDataException(String.format("bad json for OrdreAppel %d", cGpCod));
            }
            if (data[0] == null || data[0].length != 1 | data[0][0] != cGpCod)
                throw new InvalidDataException(String.format("bad json for cGpCod descriptor (expect [%d])", cGpCod));
            for(int i = 0; i < ordre_appel.size(); i++)
                if (! ordre_appel.get(i).equalsRawData(data[i+1]))
                    return false;
            return true;
        }
    }

    List<OrdreAppel> ordres_appel = new ArrayList<>();

    public void newOrdreAppel(int cGpCod) {
        OrdreAppel oa = new OrdreAppel();
        oa.cGpCod = cGpCod;
        ordres_appel.add(oa);
    }

    private OrdreAppel currentOrdreAppel() {
        return ordres_appel.get(ordres_appel.size() - 1 );
    }

    public void addCandidat(int gCnCod, int rang_appel) {
        currentOrdreAppel().addCandidat(gCnCod, rang_appel);
    }


    public void writeToPrintStream(PrintStream out) {
        writeToPrintStream(out, true);
    }

    public int[][][] getRawData() {
        int[][][] data = new int[ordres_appel.size()][][];
        for (int i = 0; i < ordres_appel.size(); i++)
            data[i] = ordres_appel.get(i).toRawData();
        return data;
    }

    public void writeToPrintStream(PrintStream out, boolean prettyPrint) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        if (prettyPrint)
            gsonBuilder = gsonBuilder.setPrettyPrinting();
        gsonBuilder.create().toJson(getRawData(), out);
    }

    public static OrdreAppelSortieBuilder fromAlgoOrdreAppelSortie(AlgoOrdreAppelSortie sortie) {
        OrdreAppelSortieBuilder result = new OrdreAppelSortieBuilder();
        for(int i = 0; i < sortie.getNbGroupes(); i++) {
            GroupeClassementValide grp = sortie.getIthGroupe(i);
            result.newOrdreAppel(grp.getCGpCod());
            fr.parcoursup.whyml.ordreappel.algo.OrdreAppel oa = grp.getOrdreAppel();
            for(int j = 0; j < oa.getNbCandidats(); j++) {
                result.addCandidat(oa.getIthGCnCod(j), oa.getIthRangAppel(j));
            }
        }
        return result;
    }

    public boolean equalsRawData(int [][][] data) throws InvalidDataException {
        if (ordres_appel.size() != data.length)
            return false;
        for(int i = 0; i < data.length; i++)
            if (! ordres_appel.get(i).equalsRawData(data[i]))
                return false;
        return true;
    }
}

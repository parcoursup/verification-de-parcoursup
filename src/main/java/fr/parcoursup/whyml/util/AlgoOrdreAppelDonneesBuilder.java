package fr.parcoursup.whyml.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import fr.parcoursup.whyml.exceptions.VerificationException;
import fr.parcoursup.whyml.ordreappel.algo.AlgoOrdreAppelEntree;
import fr.parcoursup.whyml.ordreappel.algo.AlgoOrdreAppelSortie;
import fr.parcoursup.whyml.ordreappel.algo.GroupeClassement;
import fr.parcoursup.whyml.ordreappel.algo.VoeuClasse;

import java.util.List;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.reflect.Type;

public class AlgoOrdreAppelDonneesBuilder {

    private static class Voeu {
        int gCnCod;
        int rang;
        int rangAppel;
        boolean boursier;
        boolean resident;

        int[] toRawData() {
            return new int[] { gCnCod, rang, rangAppel, boursier ? 1 : 0, resident ? 1 : 0 };
        }

        boolean equalsRawData(int [] data) {
            if (data == null || (data.length != 4 && data.length != 5))
                return false;
            if (data.length == 4)
                return gCnCod == data[0] && rang == data[1] && rangAppel == rang &&
                        (boursier == (data[2] != 0)) && (resident == (data[3] != 0));
            else
                return gCnCod == data[0] && rang == data[1] && rangAppel == data[2] &&
                        (boursier == (data[3] != 0)) && (resident == (data[4] != 0));
        }

        VoeuClasse build() throws VerificationException {
            return new VoeuClasse(new VoeuClasse(gCnCod, rang, boursier, resident),rangAppel);
        }
    }

    public interface ChangeVoeuClasseInterface {
        void startGroupe();
        VoeuClasse changeVoeuClasse(VoeuClasse v) throws VerificationException;
    }

    private static class Group {
        int cGpCod = 0;
        int tauxBoursiers = 0;
        int tauxResidents = 0;
        List<Voeu> voeux = new ArrayList<>();

        int[][] toRawData() {
            int[][] result = new int[1 + voeux.size()][];
            int offset = 0;
            result[offset++] = new int[] { cGpCod, tauxBoursiers, tauxResidents };
            for (Voeu v : voeux)
                result[offset++] = v.toRawData();
            return result;
        }

        boolean equalsRawData(int [][] data) {
            if (data == null || data.length != 1 + voeux.size() || data[0].length != 3)
                return false;
            if (data[0][0] != cGpCod || data[0][1] != tauxBoursiers || data[0][2] != tauxResidents)
                return false;
            int offset = 1;
            for(Voeu v : voeux)
                if (! v.equalsRawData(data[offset++]))
                    return false;
            return true;
        }

        GroupeClassement build() throws VerificationException {
            return build(null);
        }

        GroupeClassement build(ChangeVoeuClasseInterface chg) throws VerificationException {
            if (chg != null)
                chg.startGroupe();
            GroupeClassement result = new GroupeClassement(cGpCod, tauxBoursiers, tauxResidents);
            for (Voeu v : voeux) {
                VoeuClasse vc = v.build();
                if (chg != null)
                    vc = chg.changeVoeuClasse(vc);
                result.add(vc);
            }
            return result;
        }
    }

    private final List<Group> groups = new ArrayList<>();

    public AlgoOrdreAppelDonneesBuilder() {
    }

    public AlgoOrdreAppelDonneesBuilder(int[][][] data) throws InvalidDataException {
        this();
        for (int[][] g : data) {
            addGroupe(g);
        }
    }

    public AlgoOrdreAppelEntree buildEntree() throws VerificationException {
        AlgoOrdreAppelEntree result = new AlgoOrdreAppelEntree();
        for (Group g : groups)
            result.addGroupe(g.build());
        return result;
    }

    public AlgoOrdreAppelSortie buildSortie(ChangeVoeuClasseInterface chg) throws VerificationException {
        AlgoOrdreAppelSortie result = new AlgoOrdreAppelSortie();
        for (Group g : groups)
            result.addGroupe(g.build(chg));

        return result;
    }

    public void addGroupe(int[][] grp) throws InvalidDataException {
        if (grp == null || grp.length == 0)
            throw new InvalidDataException("null or empty group");
        if (grp[0].length != 3)
            throw new InvalidDataException("invalid group size: " + grp[0].length);
        newGroup();
        setCGpCod(grp[0][0]);
        setTauxBoursier(grp[0][1]);
        setTauxResident(grp[0][2]);
        for (int i = 1; i < grp.length; i++) {
            addVoeu(grp[i]);
        }
    }

    public void newGroup() {
        groups.add(new Group());
    }

    private Group currentGroup() {
        return groups.get(groups.size() - 1);
    }

    public void setCGpCod(int id) {
        currentGroup().cGpCod = id;
    }

    public void setTauxBoursier(int tx) {
        currentGroup().tauxBoursiers = tx;
    }

    public void setTauxResident(int tx) {
        currentGroup().tauxResidents = tx;
    }

    public void addVoeu(int gCnCod, int rang, boolean boursier, boolean resident) {
        addVoeu( gCnCod, rang, rang, boursier, resident);
    }

    public void addVoeu(int gCnCod, int rang, int rangAppel, boolean boursier, boolean resident) {
        Voeu v = new Voeu();
        v.gCnCod = gCnCod;
        v.rang = rang;
        v.rangAppel = rangAppel;
        v.boursier = boursier;
        v.resident = resident;
        currentGroup().voeux.add(v);
    }

    private void addVoeu(int[] data) throws InvalidDataException {
        if (data == null)
            throw new InvalidDataException("null voeu");
        if (data.length != 5 && data.length != 4)
            throw new InvalidDataException("invalid voeu size: " + data.length);

        if (data.length == 5)
            addVoeu(data[0], data[1], data[2], data[3] != 0, data[4] != 0);
        else
            addVoeu(data[0], data[1], data[1], data[2] != 0, data[3] != 0);
    }

    public static AlgoOrdreAppelDonneesBuilder readFromResource(String resource) throws InvalidDataException {
        InputStream in = AlgoOrdreAppelDonneesBuilder.class.getResourceAsStream(resource);
        if (in == null)
            throw new InvalidDataException("resource '" + resource + "' not found.");
        try {
            return readFromInputStream(in);
        } catch (InvalidDataException e) {
            throw new InvalidDataException("resource '" + resource + "': " + e.getMessage());
        }
    }

    public static AlgoOrdreAppelDonneesBuilder readFromInputStream(InputStream in) throws InvalidDataException {
        InputStreamReader reader = new InputStreamReader(in);
        Type t = new TypeToken<int[][][]>() {}.getType();
        int[][][] data = new Gson().fromJson(reader, t);
        if (data == null)
            throw new InvalidDataException("JSon read error");
        return new AlgoOrdreAppelDonneesBuilder(data);
    }


    public void writeToPrintStream(PrintStream out) {
        writeToPrintStream(out, true);
    }

    public void writeToPrintStream(PrintStream out, boolean prettyPrint) {
        int[][][] data = new int[groups.size()][][];
        for (int i = 0; i < groups.size(); i++)
            data[i] = groups.get(i).toRawData();
        GsonBuilder gsonBuilder = new GsonBuilder();
        if (prettyPrint)
            gsonBuilder = gsonBuilder.setPrettyPrinting();
        gsonBuilder.create().toJson(data, out);
    }

    public boolean checkAgainstRawData(int[][][] data) {
        if (data == null || data.length != groups.size())
            return false;

        for (int i = 0; i < data.length; i++) {
            if (! groups.get(i).equalsRawData(data[i]))
                return false;
        }
        return true;
    }
}

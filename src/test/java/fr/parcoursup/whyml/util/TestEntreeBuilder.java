package fr.parcoursup.whyml.util;

import com.google.gson.JsonSyntaxException;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestEntreeBuilder extends ResourceBasedTests {

    @Override
    public String getResourceDir() {
        return "/builder_test_files";
    }
    
    @Test(expected = InvalidDataException.class)
    public void testBuilderReadResourceFile() throws Exception {
        readOrdreAppelBuilder("none");
    }


    @Test(expected = InvalidDataException.class)
    public void emptyInputFile() throws Exception {
        AlgoOrdreAppelDonneesBuilder builder = readOrdreAppelBuilder("empty");

        builder.writeToPrintStream(System.out);
    }

    @Test(expected = Test.None.class)
    public void noGroups() throws Exception {
        AlgoOrdreAppelDonneesBuilder builder = readOrdreAppelBuilder("empty_array");

        assertTrue (builder.checkAgainstRawData(new int [0][][]));
    }

    @Test(expected = Test.None.class)
    public void singleGroup() throws Exception {
        AlgoOrdreAppelDonneesBuilder builder = readOrdreAppelBuilder("single_group");
        int [][][] data= new int[1][][];
        data[0] = new int[1][];
        data[0][0] = new int[] { 1, 2 ,3 } ;
        assertTrue (builder.checkAgainstRawData(data));
        data[0][0] = new int[] { 1, 2 , 3, 4 };
        assertFalse (builder.checkAgainstRawData(data));
    }

    @Test
    public void testGroupWithBadSize() throws Exception {
        InvalidDataException e =
                assertThrows(InvalidDataException.class, () ->readOrdreAppelBuilder("group_with_bad_size"));
        assertEquals(e.getMessage(),
                "resource '/builder_test_files/group_with_bad_size.json': invalid group size: 4");
    }

    @Test(expected = JsonSyntaxException.class)
    public void gsonError01() throws Exception {
        try {
            AlgoOrdreAppelDonneesBuilder builder = readOrdreAppelBuilder("gson_error_01");
        } catch (Exception e) {
            //System.err.println(e.getMessage());
            throw e;
        }
    }

    @Test(expected = JsonSyntaxException.class)
    public void gsonError02() throws Exception {
        try {
            AlgoOrdreAppelDonneesBuilder builder = readOrdreAppelBuilder("gson_error_02");
        } catch (Exception e) {
            //System.err.println(e.getMessage());
            throw e;
        }
    }

    @Test
    public void groupsWithBadSize() throws Exception {
        InvalidDataException e =
                assertThrows(InvalidDataException.class, () -> readOrdreAppelBuilder("groups_with_bad_size"));
        assertEquals(e.getMessage(), "resource '/builder_test_files/groups_with_bad_size.json': invalid voeu size: 2");
    }

    @Test(expected = Test.None.class)
    public void gsonFileOk() throws Exception {
        try {
            AlgoOrdreAppelDonneesBuilder builder = readOrdreAppelBuilder("gson_ok");
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw e;
        }
    }

}

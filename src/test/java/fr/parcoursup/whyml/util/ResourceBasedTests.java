package fr.parcoursup.whyml.util;

import fr.parcoursup.whyml.ordreappel.algo.AlgoOrdreAppelEntree;
import fr.parcoursup.whyml.ordreappel.algo.AlgoOrdreAppelSortie;

public abstract class ResourceBasedTests {
    public abstract String getResourceDir();


    protected AlgoOrdreAppelDonneesBuilder readOrdreAppelBuilder(String rsrc) throws Exception {
        return AlgoOrdreAppelDonneesBuilder.readFromResource(getResourceDir() +"/"+rsrc+".json");
    }

    protected AlgoOrdreAppelSortie readOrdreAppelSortie(String rsrc, AlgoOrdreAppelDonneesBuilder.ChangeVoeuClasseInterface chg) throws Exception {
        try {
            return readOrdreAppelBuilder(rsrc).buildSortie(chg);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw e;
        }
    }

    protected AlgoOrdreAppelSortie readOrdreAppelSortie(String rsrc) throws Exception {
        return readOrdreAppelSortie(rsrc, null);
    }

    protected AlgoOrdreAppelEntree readOrdreAppelEntree(String rsrc) throws Exception {
        try {
            return readOrdreAppelBuilder(rsrc).buildEntree();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw e;
        }
    }
}

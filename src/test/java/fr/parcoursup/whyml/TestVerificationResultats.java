package fr.parcoursup.whyml;

import fr.parcoursup.whyml.exceptions.VerificationException;
import fr.parcoursup.whyml.ordreappel.algo.AlgoOrdreAppelEntree;
import fr.parcoursup.whyml.ordreappel.algo.AlgoOrdreAppelSortie;
import fr.parcoursup.whyml.util.ResourceBasedTests;

import fr.parcoursup.whyml.verification.VerificationsResultatsAlgoOrdreAppel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

@RunWith(Parameterized.class)
public class TestVerificationResultats extends ResourceBasedTests {
    @Override
    public String getResourceDir() {
        return "/verification_resultats";
    }

    @Parameterized.Parameter(value = 0)
    public String testname;

    @Parameterized.Parameter(value = 1)
    public String inputname;

    @Parameterized.Parameter(value = 2)
    public String exceptionMessage;

    @Parameterized.Parameters(name = "{0}")
    public static Collection<Object[]> data() {
        Collection<Object[]> params = new ArrayList();
        params.add(new Object[]{"Incoherent G_CN_COD", "incoherent_g_cn_cod", "Donnees entree / sortie incoherentes G_CN_COD(E/S)=4/9" });
        params.add(new Object[]{"Missing G_CN_COD", "missing_g_cn_cod", "E/S: nombre de voeux differents 3 != 2" });
        params.add(new Object[]{"Missing groupe #1", "missing_groupe_01", "Donnee de sortie manquante" });
        params.add(new Object[]{"Missing groupe #2", "missing_groupe_02", "Tailles donnees entree / sortie incoherentes" });
        params.add(new Object[]{"No exception on success", "correct_case", null});
        params.add(new Object[]{"P1 Failure", "p1_failure", "Violation P1" });
        params.add(new Object[]{"P1 Success", "p1_success", null});
        params.add(new Object[]{"P2 Failure #1", "p2_failure_01", "Violation P2: boursier du secteur 1 depasse par 2 dans 0"});
        params.add(new Object[]{"P2 Failure #2", "p2_failure_02", "Violation P2: boursier du secteur 2 avec rang d'appel qui decroit dans 0"});
        params.add(new Object[]{"P2 Success #1", "p2_success_01", null});
        params.add(new Object[]{"P2 Success #2", "p2_success_02", null});
        params.add(new Object[]{"P3 Failure #1", "p3_failure_01", "Violation P3: non-boursier 2 depassant un candidat du secteur dans 0"});
        params.add(new Object[]{"P3 Failure #2", "p3_failure_02", "Violation P3: non boursier du secteur 2 avec rang qui diminue trop dans 0"});
        params.add(new Object[]{"P3 Success", "p3_success", null});
        params.add(new Object[]{"P4 Success", "p4_success", null});
        params.add(new Object[]{"P4 Failure #1", "p4_failure_01", "Violation P4: candidat hors-secteur 2 depassant le boursier hors-secteur 1 dans le groupe 0"});
        params.add(new Object[]{"P4 Failure #2", "p4_failure_02", "Violation P4: candidat hors-secteur boursier 2 avec rang (rp=3, ra=10) qui diminue trop dans le groupe 0"});
        params.add(new Object[]{"P5 Failure #1", "p5_failure_01", "Violation P5: candidat hors-secteur non-boursier 0 avec rang  qui diminue trop dans le groupe 0" });
        params.add(new Object[]{"P5 Failure #2", "p5_failure_02", "Violation P5: candidat hors-secteur non-boursier 0 depassant le candidat 3 dans le groupe 0" });
        params.add(new Object[]{"Verification exception on failure", "incorrect_case", "Violation P1" });
        params.add(new Object[]{"Taux min boursiers == 100 ok", "taux_min_boursiers_eq_100_ok", null });


        return params;
    }

    @Test
    public void runtime_verification_test() throws Exception {
        AlgoOrdreAppelEntree entree = readOrdreAppelEntree(inputname+"-in");
        AlgoOrdreAppelSortie sortie = readOrdreAppelSortie(inputname+"-out");
        if (exceptionMessage != null) {
            VerificationException e = assertThrows(VerificationException.class,
                    () -> VerificationsResultatsAlgoOrdreAppel.verifier(entree, sortie));
            assertEquals(exceptionMessage, e.getMessage());
        } else {
            VerificationsResultatsAlgoOrdreAppel.verifier(entree, sortie);
        }
    }

}
package fr.parcoursup.whyml;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import fr.parcoursup.whyml.ordreappel.algo.AlgoOrdreAppel;
import fr.parcoursup.whyml.ordreappel.algo.AlgoOrdreAppelEntree;
import fr.parcoursup.whyml.ordreappel.algo.AlgoOrdreAppelSortie;
import fr.parcoursup.whyml.util.OrdreAppelSortieBuilder;
import fr.parcoursup.whyml.util.ResourceBasedTests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class TestsNonRegression extends ResourceBasedTests {
    @Parameters(name = "{0}")
    public static Object[] data() {
        return new String[]{
                "example_01",
                "example_01_tx0",
                "example_02",
                "example_02_tx0",
                "example_03",
                "example_03_tx0",
                "example_04",
                "example_04_tx0",
                "example_05",
                "example_05_tx0",
                "example_06",
                "example_06_tx0",
                "example_07",
                "example_07_tx0",
                "bug_verif_p5",
                "example_08",
        };
    }

    @Parameterized.Parameter
    public String inputname;

    @Override
    public String getResourceDir() {
        return "/non_regression";
    }

    @Test
    public void nonRegressionTest() throws Exception {
        String resultrsrc = getResourceDir() + "/" + inputname + "-result.json";

        InputStream in = TestsNonRegression.class.getResourceAsStream(resultrsrc);
        if (in == null) {
            throw new Exception(String.format("missing file '%s' containing result of test %s ", resultrsrc, inputname));
        }

        AlgoOrdreAppelEntree entree = readOrdreAppelEntree(inputname);
        AlgoOrdreAppelSortie sortie = AlgoOrdreAppel.calculerOrdresAppels(entree);
        OrdreAppelSortieBuilder builder = OrdreAppelSortieBuilder.fromAlgoOrdreAppelSortie(sortie);
        //builder.writeToPrintStream(System.out, false);

        Type t = new TypeToken<int[][][]>() {}.getType();
        int[][][] expected_result = new Gson().fromJson(new InputStreamReader(in), t);
        assertTrue(builder.equalsRawData(expected_result));
    }
}

package fr.parcoursup.whyml;

import fr.parcoursup.whyml.exceptions.VerificationException;
import fr.parcoursup.whyml.util.ResourceBasedTests;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class TestErroneousDataExceptions extends ResourceBasedTests {
    @Override
    public String getResourceDir() {
        return "/erroneous_data";
    }
    
    @Test
    public void duplicatedIdsOfGroups1() {
        VerificationException e = assertThrows(VerificationException.class,
                () -> readOrdreAppelEntree("groups_with_duplicated_ids_1"));
        assertEquals(e.getMessage(),"G_CN_COD duplique 1");
    }

    @Test
    public void duplicatedIdsOfGroups2() {
        VerificationException e = assertThrows(VerificationException.class,
                () -> readOrdreAppelEntree("groups_with_duplicated_ids_2"));
        assertEquals(e.getMessage(),"G_CN_COD duplique 1142957053");
    }

    @Test
    public void wrongTauxBoursier1()  {
        VerificationException e = assertThrows(VerificationException.class,
                () -> readOrdreAppelEntree("wrong_taux_boursier_1"));
        assertEquals(e.getMessage(),"Taux boursier incoherent 757857955");
    }

    @Test
    public void wrongTauxBoursier2() {
        VerificationException e = assertThrows(VerificationException.class,
                () -> readOrdreAppelEntree("wrong_taux_boursier_2"));
        assertEquals(e.getMessage(),"Taux boursier incoherent -2141138471");
    }

    @Test
    public void wrongTauxResidents1() {
        VerificationException e = assertThrows(VerificationException.class,
                () -> readOrdreAppelEntree("wrong_taux_residents_1"));
        assertEquals(e.getMessage(),"Taux hors-secteur incoherent -2005091964");
    }

    @Test
    public void wrongTauxResidents2() {
        VerificationException e = assertThrows(VerificationException.class,
                () -> readOrdreAppelEntree("wrong_taux_residents_2"));
        assertEquals(e.getMessage(),"Taux hors-secteur incoherent -322395956");
    }

    @Test
    public void duplicatedRangVoeu() {
        VerificationException e = assertThrows(VerificationException.class,
                () -> readOrdreAppelEntree("duplicated_rang_voeu"));
        assertEquals(e.getMessage(),"G_CN_COD=363809529: rang duplique 61635558");
    }

    @Test
    public void rangVoeuIsNegative() {
        VerificationException e = assertThrows(VerificationException.class,
                () -> readOrdreAppelEntree("rang_voeu_lt_0"));
        assertEquals(e.getMessage(),"G_CN_COD=55204008 : rang negatif -1");
    }

    @Test
    public void rangVoeuIs0() {
        VerificationException e = assertThrows(VerificationException.class,
                () -> readOrdreAppelEntree("rang_voeu_eq_0"));
        assertEquals(e.getMessage(),"G_CN_COD=55204008 : rang negatif 0");
    }

    @Test
    public void duplicatedGCnCod() {
        VerificationException e = assertThrows(VerificationException.class,
                () -> readOrdreAppelEntree("duplicated_g_cn_cod"));
        assertEquals(e.getMessage(),"G_CN_COD duplique 363809529");
    }

    @Test
    public void negativeGCnCod() {
        VerificationException e = assertThrows(VerificationException.class,
                () -> readOrdreAppelEntree("negative_g_cn_cod"));
        assertEquals(e.getMessage(),"G_CN_COD=-363809529: negatif dans groupe C_GP_COD=1142957065");
    }


    @Test
    public void negativeRangAppel() {
        VerificationException e = assertThrows(VerificationException.class,
                () -> readOrdreAppelEntree("negative_rang_appel"));
        assertEquals(e.getMessage(),"G_CN_COD=0 : rang appel negatif -1");
    }

    @Test
    public void duplicatedRangAppel() {
        VerificationException e = assertThrows(VerificationException.class,
                () -> readOrdreAppelEntree("duplicated_rang_appel"));
        assertEquals(e.getMessage(),"G_CN_COD=3: rang appel duplique 1");
    }

    @Test
    public void whatsWrong() throws Exception {
        readOrdreAppelEntree("wrong");
    }
}

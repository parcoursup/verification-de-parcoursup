all : compile 

compile :
	mvn compile -Pwith-proofs

classes :
	mvn generate-sources

proofs :
	mvn generate-sources -Pwith-parallel-proofs


test:
	mvn test

deploy:
	mvn deploy -DskipTests=true -s ci_settings.xml

clean :
	mvn clean

doc:
	mvn generate-resources

jar:
	mvn package -DskipTests=true
	